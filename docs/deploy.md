# Как развернуть бота?

## Предварительные действия

1. Нужно [создать бота](create_bot.md) в Telegram и получить токен для него (переменная окружения `TELEGRAM_BOT_TOKEN`)
2. Нужно создать Google Spreadsheet и системного пользователя. Инструкции есть [здесь](google_service_account.md)
3. На сервере, где будет запущен бот – нужно установить docker и docker-compose

## Установка зависимостей

*В будущем планируется сделать полноценный билд и деплой средствами CI/CD gitlab, пока что вручную*

1. Нужно спулить содержимое репозитория;
2. Запустить команду `docker-compose run --rm composer install --prefer-dist`

## Настройка переменных окружения

1. Нужно скопировать файл `src/.env.example` в `src/.env` (если этого не произошло автоматически при запуске composer)

2. Настроить обязательно нужно следующие переменные окружения:
 - `SPREADSHEET_ID` – это ID Google Spreadsheet (часть URL, например, если ссылка на spreadsheet
 `https://docs.google.com/spreadsheets/d/XXX/edit#gid=1`, то нужно прописать так: `SPREADSHEET_ID=XXX`
 - `TELEGRAM_BOT_TOKEN` – это токен телеграм-бота, которые получается от BotFather при создании бота
 - `TELEGRAM_GROUP_ID` – ID админской группы в Telegram (в виде отрицательного числа – `-10038223`) – используется для
 того, чтобы, при обновлениях от волонтёров, отправлять сообщения в группу.
 **Бот присылает сам этот ID в ответ на команду `/start` в группе.**
 **Если ID не начинается с `-100` – значит, вы не открыли историю сообщений в этой группе – нужно обязательно открыть!**

3. Опционально, можно также настроить:
 - `SENTRY_LARAVEL_DSN` – ссылка на Sentry (используется для контроля за ошибками, и их своевременным исправлением)
 - `SENTRY_ENVIRONMENT` – окружение для Sentry (например, `DEV` или `PROD`) – чтобы было видно, откуда прилетели ошибки.

## Конфигурация сервисного аккаунта Google

После создания сервисного аккаунта – скачивается файл `google_service_account.json`, который нужно не забыть сохранить
в папку `src/config`.

## Запуск миграций

Перед запуском бота – необходимо запустить миграции, чтобы создать в Google Spreadsheet необходимую структуру данных
*(она больше для человека, который будет использовать этот файл – бот не пользуется этими заголовками)*

Детально об этом процессе написано [здесь](migrations.md)

## Запуск бота

В production-режиме бот запускается командой `docker-compose run -f docker-compose.prod.yml --rm artisan bot`.

Опции этой команды:
1. `--timeout 600` – сколько секунд демон будет висеть (по-умолчанию – 5 минут или 600 секунд).
Если настроить 0 – то будет висеть бесконечно.
*Рекомендуется настроить какой-то промежуток вроде 1 часа, и cron-расписание для запуска обработчика каждый час.*

2. `--sleepTimeout 10` – сколько секунд будет между проверкой обновлений демоном в режиме "сна" (т.е. когда за время,
определяемой опцией `waitingForUpdateTimeout` – не было обращений к боту). Чем меньше будет это значение – тем менее
"болезненным" для волонтёров будет ожидать "выход" бота из "сна".

3. `--waitingForUpdateTimeout` – период в секундах, на который бот будет "просыпаться" – если кто-то отправит ему
какую-либо команду.
