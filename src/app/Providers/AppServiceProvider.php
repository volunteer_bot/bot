<?php

namespace App\Providers;

use App\Services\AssignService;
use App\Services\InfoMessageService;
use App\Services\PublishService;
use App\Services\SheetsService;
use App\Services\TaskService;
use App\Services\VolunteerService;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Google\Client;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message as MessageObject;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, function () {
            return new Client([
                'application_name' => 'Volunteer Bot v0.1',
                'scopes' => [\Google_Service_Sheets::SPREADSHEETS],
                'credentials' => config_path('google_service_account.json'),
                'access_type' => 'offline',
                'retry' => [
                    'retries'       => 20,
                    'initial_delay' => 10,
                ],
            ]);
        });

        $this->app->singleton(\Google_Service_Sheets::class, function () {
            return new \Google_Service_Sheets(app(Client::class));
        });

        $this->app->singleton(SheetsService::class, function () {
            return new SheetsService(config('app.spreadsheetId'), app(\Google_Service_Sheets::class));
        });

        $this->app->singleton(VolunteerService::class, function () {
            return new VolunteerService(app(SheetsService::class));
        });

        Api::macro('copyMessage', function (array $params): MessageObject {
            // todo: update SDK to have the method instead of workaround
            /** @var Api $this */
            $response = $this->post('copyMessage', $params);

            return new MessageObject($response->getDecodedBody());
        });

        $this->app->singleton(PublishService::class, function (Application $app, $params) {
            // todo: make telegram SDK instance as singleton
            return new PublishService(
                app(VolunteerService::class),
                app(TaskService::class),
                app(AssignService::class),
                app(InfoMessageService::class),
                $params['telegram']
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
