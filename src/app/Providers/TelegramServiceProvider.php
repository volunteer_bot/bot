<?php

namespace App\Providers;

use Telegram\Bot\Api;
use Telegram\Bot\BotsManager;
use Telegram\Bot\Laravel\TelegramServiceProvider as BaseTelegramServiceProvider;

class TelegramServiceProvider extends BaseTelegramServiceProvider
{
    protected function registerBindings()
    {
        $this->app->singleton(BotsManager::class, static function ($app) {
            return (new BotsManager(config('telegram')))->setContainer($app);
        });
        $this->app->alias(BotsManager::class, 'telegram');

        $this->app->singleton(Api::class, static function ($app) {
            return $app[BotsManager::class]->bot();
        });
        $this->app->alias(Api::class, 'telegram.bot');
    }
}
