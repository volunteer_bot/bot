<?php

namespace App\Jobs;

use App\Helpers\TelegramHelper;
use App\Services\AssignService;
use App\Services\TaskService;
use App\Services\VolunteerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class ReplyTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $chatId;
    protected string $taskId;
    protected string $messageId;
    protected string $userName;

    /**
     * @param string $chatId
     * @param string $taskId
     * @param string $messageId
     * @param string $userName
     *
     * @return void
     */
    public function __construct(string $chatId, string $taskId, string $messageId, string $userName)
    {
        $this->chatId = $chatId;
        $this->taskId = $taskId;
        $this->messageId = $messageId;
        $this->userName = $userName;
    }

    /**
     * @param Api $telegram
     * @param VolunteerService $volunteerService
     * @param AssignService $assignService
     * @param TaskService $taskService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(
        Api $telegram,
        VolunteerService $volunteerService,
        AssignService $assignService,
        TaskService $taskService
    ) {
        if ($taskService->isFinished($this->taskId)) {
            return;
        }

        list(, $volunteer) = $volunteerService->findVolunteerByTelegramId($this->chatId);
        $volunteerId = $volunteer[0];

        $assignService->assignVolunteerToTask($volunteerId, $this->taskId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x8f\xb3\x0a " . __('telegram.you_replied'),
                            'callback_data' => 'info:replyTask',
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        $task = $taskService->getTaskById($this->taskId);

        $messageId = $task[2];

        $adminGroupId = config('telegram.groupId');

        $taskLink = "https://t.me/c/" . str_replace('-100', '', $adminGroupId) . "/{$messageId}";
        $userLink = TelegramHelper::linkByUsername($this->userName, true);

        $message = $telegram->sendMessage([
            'chat_id' => config('telegram.groupId'),
            'text' => __('telegram.new_assign_requested', [
                'volunteerId' => $volunteer[0],
                'fullName' => TelegramHelper::escapeMarkdownV2($volunteer[1]),
                'phone' => TelegramHelper::escapeMarkdownV2($volunteer[2]),
                'userLink' => $userLink,
                'taskLink' => $taskLink,
            ]),
            'parse_mode' => 'MarkdownV2',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => __('telegram.approve_assign_request'),
                            'callback_data' => "approveAssign:{$this->taskId}:{$volunteerId}",
                        ],
                    ],
                ],
            ]),
            'disable_web_page_preview' => true,
            'reply_to_message_id' => $messageId,
        ]);

        $assignService->updateAdminMessage($volunteerId, $this->taskId, $message->messageId);
    }
}
