<?php

namespace App\Jobs;

use App\Helpers\GoogleSpreadsheetsHelper;
use App\Helpers\TelegramHelper;
use App\Services\VolunteerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class ApproveVolunteer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $adminUserName;
    protected string $volunteerId;

    /**
     * @param string $adminUserName
     * @param string $volunteerId
     *
     * @return void
     */
    public function __construct(string $adminUserName, string $volunteerId)
    {
        $this->adminUserName = $adminUserName;
        $this->volunteerId = $volunteerId;
    }

    /**
     * @param Api $telegram
     * @param VolunteerService $volunteerService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, VolunteerService $volunteerService)
    {
        $telegramLink = TelegramHelper::linkByUsername($this->adminUserName);
        $adminLink = GoogleSpreadsheetsHelper::createHyperlink("@{$this->adminUserName}", $telegramLink);

        $volunteer = $volunteerService->approveVolunteer($this->volunteerId, $adminLink);

        $telegram->editMessageReplyMarkup([
            'chat_id' => config('telegram.groupId'),
            'message_id' => $volunteer[8],
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x9c\x85\x0a " . __('telegram.you_approved'),
                            'callback_data' => 'info:volunteerApproved',
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        $telegram->sendMessage([
            'chat_id' => $volunteer[4],
            'text' => __('telegram.you_have_been_approved'),
        ]);
    }
}
