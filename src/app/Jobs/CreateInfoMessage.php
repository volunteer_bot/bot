<?php

namespace App\Jobs;

use App\Helpers\GoogleSpreadsheetsHelper;
use App\Helpers\TelegramHelper;
use App\Services\InfoMessageService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class CreateInfoMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $chatId;
    protected string $messageId;
    protected string $userId;
    protected string $messageText;

    /**
     * @param string $chatId
     * @param string $messageId
     * @param string $userId
     * @param string $messageText
     *
     * @return void
     */
    public function __construct(string $chatId, string $messageId, string $userId, string $messageText)
    {
        $this->chatId = $chatId;
        $this->messageId = $messageId;
        $this->userId = $userId;
        $this->messageText = $messageText;
    }

    /**
     * @param Api $telegram
     * @param InfoMessageService $infoMessageService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, InfoMessageService $infoMessageService)
    {
        $publicChatId = str_replace('-100', '', $this->chatId);

        $user = $telegram->getChatMember([
            'chat_id' => $this->chatId,
            'user_id' => $this->userId,
        ]);

        $username = $user->user->username;
        $telegramLink = TelegramHelper::linkByUsername($username);
        $hyperlink = GoogleSpreadsheetsHelper::createHyperlink("@{$username}", $telegramLink);

        $shortMessage = mb_substr(str_replace("\n", ' ', $this->messageText), 0, 40) . '...';

        $messageLink = "https://t.me/c/{$publicChatId}/{$this->messageId}";

        $range = $infoMessageService->createInfoMessage([
            1 => $shortMessage,
            2 => GoogleSpreadsheetsHelper::createHyperlink($this->messageId, $messageLink),
            3 => $hyperlink,
            4 => Carbon::now()->toDateTimeString(),
        ]);

        $rangeLink = $infoMessageService->getInfoMessageLink($range);
        $infoMessageId = $infoMessageService->getInfoMessageIdByRange($range);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x9c\x85\x0a " . __('telegram.info_message_created'),
                            'url' => $rangeLink,
                        ],
                    ],
                    [
                        [
                            'text' => __('telegram.publish_info_message'),
                            'callback_data' => "publishInfoMessage:{$infoMessageId}",
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
