<?php

namespace App\Jobs;

use App\Services\InfoMessageService;
use App\Services\PublishService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class PublishInfoMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $infoMessageId;
    protected string $chatId;
    protected string $messageId;

    /**
     * @param string $infoMessageId
     * @param string $chatId
     * @param string $messageId
     *
     * @return void
     */
    public function __construct(string $infoMessageId, string $chatId, string $messageId)
    {
        $this->infoMessageId = $infoMessageId;
        $this->chatId = $chatId;
        $this->messageId = $messageId;
    }

    /**
     * @param Api $telegram
     * @param InfoMessageService $infoMessageService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, InfoMessageService $infoMessageService)
    {
        /** @var PublishService $publishService */
        $publishService = app(PublishService::class, [
            'telegram' => $telegram,
        ]);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x8f\xb3\x0a " . __('telegram.info_message_publishing'),
                            'url' => $infoMessageService->getRangeLinkByInfoMessageId($this->infoMessageId),
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        $publishService->publishInfoMessage($this->chatId, $this->infoMessageId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x9c\x85\x0a " . __('telegram.info_message_published'),
                            'url' => $infoMessageService->getRangeLinkByInfoMessageId($this->infoMessageId),
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
