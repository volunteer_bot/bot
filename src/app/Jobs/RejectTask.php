<?php

namespace App\Jobs;

use App\Services\AssignService;
use App\Services\VolunteerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class RejectTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $chatId;
    protected string $taskId;
    protected string $messageId;

    /**
     * @param string $chatId
     * @param string $taskId
     *
     * @return void
     */
    public function __construct(string $chatId, string $taskId, string $messageId)
    {
        $this->chatId = $chatId;
        $this->taskId = $taskId;
        $this->messageId = $messageId;
    }

    /**
     * @param Api $telegram
     * @param VolunteerService $volunteerService
     * @param AssignService $assignService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, VolunteerService $volunteerService, AssignService $assignService)
    {
        list(, $volunteer) = $volunteerService->findVolunteerByTelegramId($this->chatId);
        $volunteerId = $volunteer[0];

        $assignService->rejectTaskByVolunteer($volunteerId, $this->taskId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => __('telegram.you_rejected'),
                            'callback_data' => 'info:rejectedTask',
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
