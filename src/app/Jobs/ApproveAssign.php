<?php

namespace App\Jobs;

use App\Helpers\GoogleSpreadsheetsHelper;
use App\Helpers\TelegramHelper;
use App\Services\AssignService;
use App\Services\VolunteerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class ApproveAssign implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $adminUserName;
    protected string $volunteerId;
    protected string $taskId;
    protected string $chatId;
    protected string $messageId;

    /**
     * @param string $adminUserName
     * @param string $volunteerId
     * @param string $taskId
     * @param string $chatId
     * @param string $messageId
     *
     * @return void
     */
    public function __construct(
        string $adminUserName,
        string $volunteerId,
        string $taskId,
        string $chatId,
        string $messageId,
    ) {
        $this->adminUserName = $adminUserName;
        $this->volunteerId = $volunteerId;
        $this->taskId = $taskId;
        $this->chatId = $chatId;
        $this->messageId = $messageId;
    }

    /**
     * @param Api $telegram
     * @param VolunteerService $volunteerService
     * @param AssignService $assignService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, VolunteerService $volunteerService, AssignService $assignService)
    {
        $telegramLink = TelegramHelper::linkByUsername($this->adminUserName);
        $adminLink = GoogleSpreadsheetsHelper::createHyperlink("@{$this->adminUserName}", $telegramLink);

        list(, $volunteer) = $volunteerService->findVolunteerById($this->volunteerId);

        $assignRequest = $assignService->approveVolunteerRequest($this->volunteerId, $this->taskId, $adminLink);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $volunteer[4],
            'message_id' => $assignRequest[5],
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x9c\x85\x0a " . __('telegram.your_request_approved'),
                            'callback_data' => 'info:requestApproved',
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        $telegram->sendMessage([
            'chat_id' => $volunteer[4],
            'text' => __('telegram.your_request_approved_full'),
            'reply_to_message_id' => $assignRequest[5],
        ]);

        $telegram->editMessageReplyMarkup([
            'chat_id'    => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x9c\x85\x0a " . __('telegram.your_request_approved'),
                            'callback_data' => "info:requestApprovedForAdmin",
                        ],
                    ],
                ],
            ]),
        ]);
    }
}
