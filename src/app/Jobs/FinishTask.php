<?php

namespace App\Jobs;

use App\Services\PublishService;
use App\Services\TaskService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class FinishTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $taskId;
    protected string $chatId;
    protected string $messageId;


    /**
     * @param string $taskId
     * @param string $chatId
     * @param string $messageId
     *
     * @return void
     */
    public function __construct(string $taskId, string $chatId, string $messageId)
    {
        $this->taskId = $taskId;
        $this->chatId = $chatId;
        $this->messageId = $messageId;
    }

    /**
     * @param Api $telegram
     * @param TaskService $taskService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, TaskService $taskService)
    {
        /** @var PublishService $publishService */
        $publishService = app(PublishService::class, [
            'telegram' => $telegram,
        ]);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x8f\xb3\x0a " . __('telegram.task_finishing'),
                            'url' => $taskService->getRangeLinkByTaskId($this->taskId),
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        // TODO: обновление всех запросов в админской группе после финиша таски.

        $publishService->finishTask($this->taskId, $this->chatId);

        $taskService->updateFinishedAtFieldForTask($this->taskId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xf0\x9f\x8f\x81 " . __('telegram.task_finished'),
                            'url' => $taskService->getRangeLinkByTaskId($this->taskId),
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
