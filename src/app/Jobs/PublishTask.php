<?php

namespace App\Jobs;

use App\Services\PublishService;
use App\Services\TaskService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class PublishTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $taskId;
    protected string $chatId;
    protected string $messageId;

    public function __construct(string $taskId, string $chatId, string $messageId)
    {
        $this->taskId = $taskId;
        $this->chatId = $chatId;
        $this->messageId = $messageId;
    }

    /**
     * @param Api $telegram
     * @param TaskService $taskService
     *
     * @throws TelegramSDKException
     *
     * @return void
     */
    public function handle(Api $telegram, TaskService $taskService): void
    {
        $publishService = app(PublishService::class, [
            'telegram' => $telegram,
        ]);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x8f\xb3\x0a " . __('telegram.task_publishing'),
                            'url' => $taskService->getRangeLinkByTaskId($this->taskId),
                        ],
                    ],
                    [
                        [
                            'text' => __('telegram.finish_task'),
                            'callback_data' => "finishTask:{$this->taskId}",
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        $publishService->publishTask($this->chatId, $this->taskId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x9c\x85\x0a " . __('telegram.task_published'),
                            'url' => $taskService->getRangeLinkByTaskId($this->taskId),
                        ],
                    ],
                    [
                        [
                            'text' => __('telegram.finish_task'),
                            'callback_data' => "finishTask:{$this->taskId}",
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
