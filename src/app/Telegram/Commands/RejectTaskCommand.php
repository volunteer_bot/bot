<?php

namespace App\Telegram\Commands;

use App\Jobs\RejectTask;

class RejectTaskCommand extends VirtualCommand
{
    public $name = 'rejectTask';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.reject_task_message'),
            'show_alert' => true,
        ]);

        RejectTask::dispatch(
            $this->update->getChat()->id,
            $this->entity['taskId'],
            $callbackQuery->message->messageId,
        );
    }
}
