<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram\Bot\Commands\HelpCommand as BaseHelpCommand;

class HelpCommand extends BaseHelpCommand
{
    public function getDescription(): string
    {
        return __('telegram.help_command_description');
    }

    public function handle()
    {
        $commands = $this->telegram->getCommands();

        $text = '';
        foreach ($commands as $name => $handler) {
            if (!$handler instanceof VirtualCommand) {
                /* @var Command $handler */
                $text .= sprintf('/%s - %s' . PHP_EOL, $name, $handler->getDescription());
            }
        }

        $this->replyWithMessage(compact('text'));
    }
}
