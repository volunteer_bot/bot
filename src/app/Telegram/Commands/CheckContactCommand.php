<?php

namespace App\Telegram\Commands;

use App\Helpers\GoogleSpreadsheetsHelper;
use App\Helpers\TelegramHelper;
use App\Services\VolunteerService;
use Carbon\Carbon;
use Telegram\Bot\Objects\Contact;

class CheckContactCommand extends VirtualCommand
{
    public $name = 'checkContact';

    public function handle()
    {
        /** @var VolunteerService $volunteerService */
        $volunteerService = app(VolunteerService::class);

        /** @var Contact $contact */
        $contact = $this->update->getMessage()->get('contact');

        $username = $this->getUpdate()->getChat()->username;
        $telegramLink = TelegramHelper::linkByUsername($username);
        $hyperlink = GoogleSpreadsheetsHelper::createHyperlink("@{$username}", $telegramLink);

        if ($volunteerService->isVolunteerApproved($contact->phoneNumber)) {
            $this->replyWithMessage([
                'text' => __('telegram.volunteer_found'),
                'reply_markup' => json_encode([
                    'remove_keyboard' => true,
                ]),
            ]);

            $volunteerService->updateVolunteer($contact->phoneNumber, [
                2 => $contact->phoneNumber,
                3 => $hyperlink,
                4 => intval($contact->userId),
            ]);
        } else {
            $this->replyWithMessage([
                'text' => __('telegram.volunteer_not_found'),
                'reply_markup' => json_encode([
                    'remove_keyboard' => true,
                ]),
            ]);

            if (!$volunteerService->isVolunteerExists($contact->phoneNumber)) {
                $fullName = "{$contact->firstName} {$contact->lastName}";

                $volunteerService->createVolunteer([
                    1 => $fullName,
                    2 => $contact->phoneNumber,
                    // todo: replace with tg://user?id=<id> link
                    3 => $hyperlink,
                    4 => intval($contact->userId),
                    5 => Carbon::now()->toDateTimeString(),
                ]);

                [,$volunteer] = $volunteerService->findVolunteerByPhone($contact->phoneNumber);

                $message = $this->telegram->sendMessage([
                    'chat_id' => config('telegram.groupId'),
                    'text' => __('telegram.new_volunteer', [
                        'volunteerId' => $volunteer[0],
                        'fullName' => TelegramHelper::escapeMarkdownV2($fullName),
                        'phone' => TelegramHelper::escapeMarkdownV2($contact->phoneNumber),
                        'userLink' => TelegramHelper::linkByUsername($username, true),
                    ]),
                    'parse_mode' => 'MarkdownV2',
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => __('telegram.approve_volunteer'),
                                    'callback_data' => "approveVolunteer:{$volunteer[0]}",
                                ],
                            ],
                        ],
                    ]),
                ]);

                $volunteerService->addApproveVolunteerMessageId($volunteer[0], $message->messageId);
            }
        }
    }
}
