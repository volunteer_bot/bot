<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\CreateInfoMessage;
use App\Telegram\Commands\VirtualCommand;

class CreateInfoMessageCommand extends VirtualCommand
{
    public $name = 'createInfoMessage';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.info_message_creation_in_progress'),
            'show_alert' => true,
        ]);

        CreateInfoMessage::dispatch(
            $callbackQuery->message->chat->id,
            $callbackQuery->message->messageId,
            $this->entity['userId'],
            $callbackQuery->message->text,
        );
    }
}
