<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Telegram\Commands\VirtualCommand;
use Illuminate\Support\Collection;
use Telegram\Bot\Objects\MessageEntity;

class SuperGroupCommand extends VirtualCommand
{
    public $name = 'supergroup';

    private $bot;

    public function handle()
    {
        // todo: cache the bot itself
        $this->bot = $this->telegram->getMe();

        if ($this->isBotMentioned()) {
            $this->resendMessageWithActions();
        }
    }

    private function resendMessageWithActions(): void
    {
        $userId = $this->update->message->from->id;

        $inlineKeyboard = [
            [
                [
                    'text' => __('telegram.create_task'),
                    'callback_data' => "createTask:{$userId}",
                ],
            ],
            [
                [
                    'text' => __('telegram.create_info_message'),
                    'callback_data' => "createInfoMessage:{$userId}",
                ],
            ],
        ];

        $this->replyWithMessage([
            'text' => $this->messageWithoutBotMention(),
            'reply_markup' => json_encode([
                'inline_keyboard' => $inlineKeyboard,
                'resize_keyboard' => true,
            ]),
        ]);

        $this->telegram->deleteMessage([
            'chat_id' => $this->update->message->chat->id,
            'message_id' => $this->update->message->messageId,
        ]);
    }

    private function messageWithoutBotMention(): string
    {
        $text = $this->getUpdate()->getMessage()->text;

        return ltrim(str_replace("@{$this->bot->username}", '', $text));
    }

    private function isBotMentioned(): bool
    {
        /** @var Collection|MessageEntity[] $entities */
        $entities = $this->getUpdate()->getMessage()->entities;

        if (empty($entities) || $entities->count() === 0) {
            return false;
        }

        $text = $this->getUpdate()->getMessage()->text;

        foreach ($entities as $entity) {
            if (!$entity instanceof MessageEntity) {
                // workaround for SDK bug
                $entity = new MessageEntity($entity);
            }

            if ($entity->type === 'mention') {
                $mentionedUserName = substr($text, $entity->offset, $entity->length);

                if ($mentionedUserName === "@{$this->bot->username}") {
                    return true;
                }
            }
        }

        return false;
    }
}
