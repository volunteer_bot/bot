<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\CreateTask;
use App\Telegram\Commands\VirtualCommand;

class CreateTaskCommand extends VirtualCommand
{
    public $name = 'createTask';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.task_creation_in_progress'),
            'show_alert' => true,
        ]);

        CreateTask::dispatch(
            $callbackQuery->message->chat->id,
            $callbackQuery->message->messageId,
            $this->entity['userId'],
            $callbackQuery->message->text
        );
    }
}
