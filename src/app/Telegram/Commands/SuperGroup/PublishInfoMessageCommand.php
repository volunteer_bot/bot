<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\PublishInfoMessage;
use App\Telegram\Commands\VirtualCommand;

class PublishInfoMessageCommand extends VirtualCommand
{
    public $name = 'publishInfoMessage';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.info_message_publication_in_progress'),
            'show_alert' => true,
        ]);

        PublishInfoMessage::dispatch(
            $this->entity['messageId'],
            $callbackQuery->message->chat->id,
            $callbackQuery->message->messageId,
        );
    }
}
