<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\FinishTask;
use App\Telegram\Commands\VirtualCommand;

class FinishTaskCommand extends VirtualCommand
{
    public $name = 'finishTask';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.task_finish_in_progress'),
            'show_alert' => true,
        ]);

        FinishTask::dispatch(
            $this->entity['taskId'],
            $callbackQuery->message->chat->id,
            $callbackQuery->message->messageId,
        );
    }
}
