<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\PublishTask;
use App\Telegram\Commands\VirtualCommand;

class PublishTaskCommand extends VirtualCommand
{
    public $name = 'publishTask';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.task_publication_in_progress'),
            'show_alert' => true,
        ]);

        PublishTask::dispatch(
            $this->entity['taskId'],
            $callbackQuery->message->chat->id,
            $callbackQuery->message->messageId
        );
    }
}
