<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\ApproveAssign;
use App\Telegram\Commands\VirtualCommand;

class ApproveAssignCommand extends VirtualCommand
{
    public $name = 'approveAssign';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.approving_assign_request'),
            'show_alert' => true,
        ]);

        ApproveAssign::dispatch(
            $callbackQuery->from->username,
            $this->entity['volunteerId'],
            $this->entity['taskId'],
            $callbackQuery->message->chat->id,
            $callbackQuery->message->messageId,
        );
    }
}
