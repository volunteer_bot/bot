<?php

namespace App\Telegram\Commands\SuperGroup;

use App\Jobs\ApproveVolunteer;
use App\Telegram\Commands\VirtualCommand;

class ApproveVolunteerCommand extends VirtualCommand
{
    public $name = 'approveVolunteer';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.approving_volunteer'),
            'show_alert' => true,
        ]);

        ApproveVolunteer::dispatch($callbackQuery->from->username, $this->entity['userId']);
    }
}
