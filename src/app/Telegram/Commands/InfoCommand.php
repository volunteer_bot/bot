<?php

namespace App\Telegram\Commands;

class InfoCommand extends VirtualCommand
{
    public $name = 'info';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __("telegram.info.{$this->entity['message']}"),
            'show_alert' => true,
            'cache_time' => 3600,
        ]);
    }
}
