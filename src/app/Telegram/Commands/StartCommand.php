<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class StartCommand extends Command
{
    protected $name = 'start';

    public function getDescription(): string
    {
        return __('telegram.start_description');
    }

    public function handle()
    {
        if ($this->update->getChat()->type !== 'private') {
            $chatId = $this->update->getChat()->id;

            $this->replyWithMessage([
                'text' => __('telegram.group_id_info', [
                    'groupId' => $chatId,
                ]),
                'parse_mode' => 'MarkdownV2',
            ]);

            return;
        }

        $requestContactButton = Keyboard::button([
            'text' => __('telegram.get_contact'),
            'request_contact' => true,
        ]);

        $this->replyWithMessage([
            'text'         => __('telegram.introduction'),
            'reply_markup' => json_encode([
                'keyboard' => [
                    [ $requestContactButton ],
                ],
                'one_time_keyboard' => true,
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
