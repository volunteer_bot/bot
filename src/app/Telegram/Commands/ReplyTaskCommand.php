<?php

namespace App\Telegram\Commands;

use App\Jobs\ReplyTask;

class ReplyTaskCommand extends VirtualCommand
{
    public $name = 'replyTask';

    public function handle()
    {
        $callbackQuery = $this->update->callbackQuery;

        $this->telegram->answerCallbackQuery([
            'callback_query_id' => $callbackQuery->id,
            'text' => __('telegram.reply_task_connect_message'),
            'show_alert' => true,
        ]);

        ReplyTask::dispatch(
            $this->update->getChat()->id,
            $this->entity['taskId'],
            $callbackQuery->message->messageId,
            $this->update->getChat()->username
        );
    }
}
