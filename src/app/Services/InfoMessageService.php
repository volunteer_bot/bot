<?php

namespace App\Services;

use Carbon\Carbon;

class InfoMessageService
{
    private SheetsService $sheetsService;
    private string $sheetName;
    private array $columns;

    public function __construct(SheetsService $sheetsService)
    {
        $this->sheetsService = $sheetsService;
        $this->sheetName = config('sheets.info_messages.sheetName');
        $this->columns = config('sheets.info_messages.columns');
    }

    public function createInfoMessage(array $data): string
    {
        array_unshift($data, '=ROW()-1');
        $data[5] = '';

        $range = $this->sheetsService->addRow($this->sheetName, ...$data);

        return $range;
    }

    public function getRangeLinkByInfoMessageId(string $infoMessageId): string
    {
        $tasks = $this->sheetsService->getValues($this->sheetName, 1);

        foreach ($tasks as $row => $task) {
            if ($task[0] == $infoMessageId) {
                break;
            }
        }

        $range = $this->sheetsService->generateRangeForColumnsByRow($this->sheetName, $row, count($this->columns));

        return $this->sheetsService->getLinkToRange($this->sheetName, $range);
    }

    public function getInfoMessageById(string $messageId): ?array
    {
        $infoMessages = $this->sheetsService->getValues($this->sheetName, count($this->columns));

        foreach ($infoMessages as $row => $infoMessage) {
            if ($infoMessage[0] == $messageId) {
                return $infoMessage;
            }
        }

        return null;
    }

    public function updatePublishedAtFieldForInfoMessage(string $infoMessageId): void
    {
        $this->setValue($infoMessageId, 5, Carbon::now()->toDateTimeString());
    }

    public function getInfoMessageLink(string $range): string
    {
        return $this->sheetsService->getLinkToRange($this->sheetName, $range);
    }

    public function getInfoMessageIdByRange(string $range): string
    {
        $values = $this->sheetsService->getValuesByRange($this->sheetName, $range);

        return $values[0][0];
    }

    protected function setValue(string $infoMessageId, int $columnIdx, string $value): void
    {
        $tasks = $this->sheetsService->getValues($this->sheetName, 1);

        foreach ($tasks as $row => $task) {
            if ($task[0] == $infoMessageId) {
                break;
            }
        }

        $this->sheetsService->updateCell($this->sheetName, $row, $columnIdx, $value);
    }
}
