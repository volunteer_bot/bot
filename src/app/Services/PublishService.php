<?php

namespace App\Services;

use Throwable;
use JetBrains\PhpStorm\Pure;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use function Sentry\captureException;

class PublishService
{
    protected $telegram;
    protected $volunteerService;
    protected $taskService;
    protected $assignService;
    protected $infoMessageService;

    public function __construct(
        VolunteerService $volunteerService,
        TaskService $taskService,
        AssignService $assignService,
        InfoMessageService $infoMessageService,
        Api $telegram
    ) {
        $this->volunteerService = $volunteerService;
        $this->taskService = $taskService;
        $this->assignService = $assignService;
        $this->infoMessageService = $infoMessageService;
        $this->telegram = $telegram;
    }

    public function publishTask(string $chatId, string $taskId, int $startFrom = 1)
    {
        $task = $this->taskService->getTaskById($taskId);

        $messageId = $task[2];

        $volunteers = $this->volunteerService->findVolunteers();

        foreach ($volunteers as $volunteer) {
            if (empty($volunteer[4])) {
                echo "Skip deleted volunteer" . PHP_EOL;
                continue;
            }

            if (empty($volunteer[7])) {
                echo "Skip not-approved volunteer" . PHP_EOL;
                continue;
            }

            if ($volunteer[0] < $startFrom) {
                echo "Skip user because ID less then startFrom" . PHP_EOL;
                continue;
            }

            try {
                $message = $this->telegram->copyMessage([
                    'chat_id' => $volunteer[4],
                    'from_chat_id' => $chatId,
                    'message_id' => $messageId,
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => __('telegram.reply_task'),
                                    'callback_data' => "replyTask:{$taskId}",
                                ],
                            ],
                            [
                                [
                                    'text' => __('telegram.reject_task'),
                                    'callback_data' => "rejectTask:{$taskId}",
                                ],
                            ],
                        ],
                        'resize_keyboard' => true,
                    ]),
                ]);

                $this->assignService->sentRequestToVolunteer($volunteer, $taskId, $message->get('message_id'));
            } catch (TelegramSDKException $e) {
                if ($this->isBotBlocked($e)) {
                    echo "Bot was blocked by volunteer" . PHP_EOL;
                    continue;
                }

                captureException($e);
            } catch (Throwable $e) {
                captureException($e);
            }
        }

        $this->taskService->updatePublishedAtFieldForTask($taskId);
    }

    public function finishTask(string $taskId, string $adminChatId, int $startFrom = 1)
    {
        $volunteers = $this->volunteerService->findVolunteers();

        foreach ($volunteers as $volunteer) {
            if (empty($volunteer[4])) {
                echo "Skip deleted user" . PHP_EOL;
                continue;
            }

            if (empty($volunteer[7])) {
                echo "Skip not-approved volunteer" . PHP_EOL;
                continue;
            }

            if ($volunteer[0] < $startFrom) {
                echo "Skip user because ID less then startFrom" . PHP_EOL;
                continue;
            }

            [, $assignRequest] = $this->assignService->findAssignRequest($volunteer[0], $taskId);

            if (empty($assignRequest)) {
                echo "Assign request is not found (volunteer has blocked bot)!" . PHP_EOL;
                continue;
            }

            try {
                $this->telegram->editMessageReplyMarkup([
                    'chat_id' => $volunteer[4],
                    'message_id' => $assignRequest[5],
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => "\xf0\x9f\x8f\x81 " . __('telegram.task_has_been_finished'),
                                    'callback_data' => 'info:taskFinished',
                                ],
                            ],
                        ],
                        'resize_keyboard' => true,
                    ]),
                ]);
            } catch (TelegramSDKException $e) {
                if ($this->isBotBlocked($e)) {
                    echo "Bot was blocked by volunteer" . PHP_EOL;
                    continue;
                }

                captureException($e);
            } catch (Throwable $e) {
                captureException($e);
            }

            if (!empty($assignRequest[6])) {
                $this->telegram->editMessageReplyMarkup([
                    'chat_id' => $adminChatId,
                    'message_id' => $assignRequest[6],
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => "\xf0\x9f\x8f\x81 " . __('telegram.task_has_been_finished'),
                                    'callback_data' => 'info:taskFinishedForAdmin',
                                ],
                            ],
                        ],
                        'resize_keyboard' => true,
                    ]),
                ]);
            }
        }
    }

    public function publishInfoMessage(string $chatId, string $infoMessageId, int $startFrom = 1)
    {
        $infoMessage = $this->infoMessageService->getInfoMessageById($infoMessageId);

        $messageId = $infoMessage[2];

        $volunteers = $this->volunteerService->findVolunteers();

        foreach ($volunteers as $volunteer) {
            if (empty($volunteer[4])) {
                echo "Skip deleted user" . PHP_EOL;
                continue;
            }

            if (empty($volunteer[7])) {
                echo "Skip not-approved volunteer" . PHP_EOL;
                continue;
            }

            if ($volunteer[0] < $startFrom) {
                echo "Skip user because ID less then startFrom" . PHP_EOL;
                continue;
            }

            try {
                $this->telegram->copyMessage([
                    'chat_id' => $volunteer[4],
                    'from_chat_id' => $chatId,
                    'message_id' => $messageId,
                ]);
            }  catch (TelegramSDKException $e) {
                if ($this->isBotBlocked($e)) {
                    echo "Bot was blocked by volunteer" . PHP_EOL;
                    continue;
                }

                captureException($e);
            } catch (Throwable $e) {
                captureException($e);
            }
        }

        $this->infoMessageService->updatePublishedAtFieldForInfoMessage($infoMessageId);
    }

    #[Pure]
    protected function isBotBlocked(TelegramSDKException $e): bool
    {
        return str_contains($e->getMessage(), 'bot was blocked');
    }
}
