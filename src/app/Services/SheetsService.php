<?php

namespace App\Services;

use JetBrains\PhpStorm\Pure;

class SheetsService
{
    const SPREADSHEET_BASE_URL = 'https://docs.google.com/spreadsheets/d/';

    private \Google_Service_Sheets $googleService;
    private string $spreadsheetId;
    private array $cachedSheetsIds;

    public function __construct(string $sheetId, \Google_Service_Sheets $googleService)
    {
        $this->googleService = $googleService;
        $this->spreadsheetId = $sheetId;
    }

    public function createOrUpdateSheet(string $sheetName, string ...$columns)
    {
        if (!$this->isSheetExists($sheetName)) {
            $sheet = $this->createSheet($sheetName);

            echo "Created Sheet #{$sheet->getProperties()->getSheetId()} (`{$sheetName}`)\n";

            $protectedRangeId = $this->protectHeaders($sheet->getProperties()->getSheetId(), count($columns));

            echo "Created protected range #{$protectedRangeId} (`{$sheetName}`)\n";
        }

        $this->fillSheetHeaders($sheetName, ...$columns);
    }

    public function getValues(string $sheetName, int $columnsCount): array
    {
        return $this->getValuesByRange($sheetName, 'A:' . $this->convertColumnAddress($columnsCount - 1));
    }

    public function getValuesByRange(string $sheetName, string $range): array
    {
        if (!str_contains($range, $sheetName)) {
            $range = "'{$sheetName}'!{$range}";
        }

        $response = $this->googleService->spreadsheets_values->get($this->spreadsheetId, $range);

        return $response->getValues();
    }

    public function batchGetValues(array $sheets): array
    {
        $ranges = [];

        foreach ($sheets as $sheetKey => $sheet) {
            $sheetName = $sheet['sheetName'];
            $ranges[$sheetKey] = "{$sheetName}!A:" . $this->convertColumnAddress(count($sheet['columns']) - 1);
        }

        $response = $this->googleService->spreadsheets_values->batchGet($this->spreadsheetId, [
            'ranges' => $ranges,
        ]);

        $result = [];
        $sheetKeys = array_keys($sheets);

        foreach ($response->getValueRanges() as $idx => $valueRange) {
            $result[$sheetKeys[$idx]] = $valueRange->getValues();
        }

        return $result;
    }

    protected function fillSheetHeaders(string $sheetName, string ...$columns)
    {
        $this->updateRow($sheetName, 0, ...$columns);
    }

    public function hideColumn(string $sheetName, int $columnIdx): void
    {
        $sheetId = $this->getSheetId($sheetName);

        $updateDimensionPropertiesRequest = new \Google_Service_Sheets_UpdateDimensionPropertiesRequest([
            'range' => new \Google_Service_Sheets_DimensionRange([
                'sheetId'       => $sheetId,
                'startIndex'    => $columnIdx,
                'endIndex'      => $columnIdx + 1,
                'dimension'     => 'COLUMNS',
            ]),
            'fields'     => '*',
            'properties' => new \Google_Service_Sheets_DimensionProperties([
                'hiddenByUser' => true,
            ]),
        ]);

        $batchRequests = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest();

        $batchRequests->setRequests([
            new \Google_Service_Sheets_Request([
                'updateDimensionProperties' => $updateDimensionPropertiesRequest,
            ]),
        ]);

        $this->googleService->spreadsheets->batchUpdate($this->spreadsheetId, $batchRequests);
    }

    public function updateRow(string $sheetName, int $rowIdx, string ...$row)
    {
        $range = $this->generateRangeForColumnsByRow($sheetName, $rowIdx, count($row));

        $valueRange = new \Google_Service_Sheets_ValueRange([
            'range'  => $range,
            'values' => [$row],
            'majorDimension' => 'ROWS',
        ]);

        $this->googleService->spreadsheets_values->update($this->spreadsheetId, $range, $valueRange, [
            'valueInputOption' => 'USER_ENTERED',
            'includeValuesInResponse' => false,
        ]);
    }

    public function updateCell(string $sheetName, int $rowIdx, int $columnIdx, string $value)
    {
        $range = "{$sheetName}!" . $this->generateRange($rowIdx, $columnIdx, $rowIdx, $columnIdx);

        $valueRange = new \Google_Service_Sheets_ValueRange([
            'range'  => $range,
            'values' => [[$value]],
            'majorDimension' => 'ROWS',
        ]);

        $this->googleService->spreadsheets_values->update($this->spreadsheetId, $range, $valueRange, [
            'valueInputOption' => 'USER_ENTERED',
            'includeValuesInResponse' => false,
        ]);
    }

    public function addRow(string $sheetName, string ...$row)
    {
        $range = "{$sheetName}!" . $this->convertColumnAddress(0) . ':' . $this->convertColumnAddress(count($row) - 1);

        $valueRange = new \Google_Service_Sheets_ValueRange([
            'range'  => $range,
            'values' => [$row],
            'majorDimension' => 'ROWS',
        ]);

        $response = $this->googleService->spreadsheets_values->append($this->spreadsheetId, $range, $valueRange, [
            'valueInputOption' => 'USER_ENTERED',
            'includeValuesInResponse' => false,
        ]);

        return $response->getUpdates()->getUpdatedRange();
    }

    public function getSheetId(string $sheetName): ?string
    {
        if (isset($this->cachedSheetsIds[$sheetName])) {
            return $this->cachedSheetsIds[$sheetName];
        }

        $spreadsheet = $this->googleService->spreadsheets->get($this->spreadsheetId, [
            'fields' => 'sheets.properties',
        ]);

        $sheets = $spreadsheet->getSheets();

        foreach ($sheets as $sheet) {
            if ($sheet->getProperties()->getTitle() === $sheetName) {
                return $this->cachedSheetsIds[$sheetName] = $sheet->getProperties()->getSheetId();
            }
        }

        return null;
    }

    public function generateRangeForColumnsByRow(string $sheetName, int $row, int $columnsCount): string
    {
        return "{$sheetName}!" . $this->generateRange($row, 0, $row, $columnsCount - 1);
    }

    public function getLinkToRange(string $sheetName, string $range): string
    {
        $sheetId = $this->getSheetId($sheetName);

        if (str_contains($range, '!')) {
            list (, $range) = explode('!', $range);
        }

        return self::SPREADSHEET_BASE_URL . $this->spreadsheetId . "/edit#gid={$sheetId}?range={$range}";
    }

    protected function createSheet(string $sheetName)
    {
        $batchRequests = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest();

        $batchRequests->setRequests([
            new \Google_Service_Sheets_Request([
                'addSheet' => new \Google_Service_Sheets_AddSheetRequest([
                    'properties' => new \Google_Service_Sheets_SheetProperties([
                        'title' => $sheetName,
                    ]),
                ]),
            ]),
        ]);

        $responses = $this->googleService->spreadsheets->batchUpdate($this->spreadsheetId, $batchRequests);

        return $responses->getReplies()[0]->getAddSheet();
    }

    public function protectHeaders(string $sheetId, int $headersCount)
    {
        $protectedRange = new \Google_Service_Sheets_ProtectedRange([
            'range' => new \Google_Service_Sheets_GridRange([
                'sheetId'          => $sheetId,
                'startRowIndex'    => 0,
                'endRowIndex'      => 1,
                'startColumnIndex' => 0,
                'endColumnIndex'   => $headersCount,
            ]),
            'warningOnly' => false,
            'requestingUserCanEdit' => false,
            'editors' => [
                'users' => [
                    $this->googleService->getClient()->getConfig('client_email'),
                ],
            ],
        ]);

        $batchRequests = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest();

        $batchRequests->setRequests([
            new \Google_Service_Sheets_Request([
                'addProtectedRange' => new \Google_Service_Sheets_AddProtectedRangeRequest([
                    'protectedRange' => $protectedRange,
                ]),
            ]),
        ]);

        $responses = $this->googleService->spreadsheets->batchUpdate($this->spreadsheetId, $batchRequests);

        $response = $responses->getReplies()[0]->getAddProtectedRange();

        return $response->getProtectedRange()->getProtectedRangeId();
    }

    public function isSheetExists(string $sheetName): bool
    {
        return $this->getSheetId($sheetName) > 0;
    }

    #[Pure]
    private function generateRange(int $fromRow, int $fromColumn, int $toRow, int $toColumn): string
    {
        return $this->convertCellAddress($fromRow, $fromColumn) . ':' . $this->convertCellAddress($toRow, $toColumn);
    }

    private array $columns = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
        'W', 'X', 'Y', 'Z',
    ];

    #[Pure]
    private function convertCellAddress(int $row, int $column): string
    {
        return $this->convertColumnAddress($column) . ($row + 1);
    }

    #[Pure]
    private function convertColumnAddress(int $column): string
    {
        $column += 1;

        $letter = '';

        while ($column != 0) {
            $p = ($column - 1) % 26;
            $column = intval(($column - $p) / 26);
            $letter = $this->columns[$p] . $letter;
        }

        return $letter;
    }
}
