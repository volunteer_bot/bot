<?php

namespace App\Services;

use Carbon\Carbon;

class TaskService
{
    private SheetsService $sheetsService;
    private string $sheetName;
    private array $columns;

    public function __construct(SheetsService $sheetsService)
    {
        $this->sheetsService = $sheetsService;
        $this->sheetName = config('sheets.tasks.sheetName');
        $this->columns = config('sheets.tasks.columns');
    }

    public function createTask(array $data)
    {
        array_unshift($data, '=ROW()-1');
        $data[5] = $data[6] = '';

        $range = $this->sheetsService->addRow($this->sheetName, ...$data);

        return $range;
    }

    public function getRangeLinkByTaskId(string $taskId): string
    {
        $tasks = $this->sheetsService->getValues($this->sheetName, 1);

        foreach ($tasks as $row => $task) {
            if ($task[0] == $taskId) {
                break;
            }
        }

        $range = $this->sheetsService->generateRangeForColumnsByRow($this->sheetName, $row, count($this->columns));

        return $this->sheetsService->getLinkToRange($this->sheetName, $range);
    }

    public function getTaskById(string $taskId): ?array
    {
        $tasks = $this->sheetsService->getValues($this->sheetName, count($this->columns));

        foreach ($tasks as $row => $task) {
            if ($task[0] == $taskId) {
                return $task;
            }
        }

        return null;
    }

    public function isFinished(string $taskId): bool
    {
        $task = $this->getTaskById($taskId);

        return !empty($task[6]);
    }

    public function updateFinishedAtFieldForTask(string $taskId): void
    {
        $this->setValue($taskId, 6, Carbon::now()->toDateTimeString());
    }

    public function updatePublishedAtFieldForTask(string $taskId): void
    {
        $this->setValue($taskId, 5, Carbon::now()->toDateTimeString());
    }

    protected function setValue(string $taskId, int $columnIdx, string $value): void
    {
        $tasks = $this->sheetsService->getValues($this->sheetName, 1);

        foreach ($tasks as $row => $task) {
            if ($task[0] == $taskId) {
                break;
            }
        }

        $this->sheetsService->updateCell($this->sheetName, $row, $columnIdx, $value);
    }

    public function getTaskLink(string $range): string
    {
        return $this->sheetsService->getLinkToRange($this->sheetName, $range);
    }

    public function getTaskIdByRange(string $range): string
    {
        $values = $this->sheetsService->getValuesByRange($this->sheetName, $range);

        return $values[0][0];
    }
}
