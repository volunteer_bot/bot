<?php

namespace App\Services;

use App\Helpers\GoogleSpreadsheetsHelper;
use App\Helpers\TelegramHelper;
use Carbon\Carbon;

class AssignService
{
    private $sheetsService;
    private $sheetName;
    private $columns;

    public function __construct(SheetsService $sheetsService)
    {
        $this->sheetsService = $sheetsService;
        $this->sheetName = config('sheets.assigns.sheetName');
        $this->columns = config('sheets.assigns.columns');
    }

    public function sentRequestToVolunteer(array $volunteer, string $taskId, string $messageId)
    {
        $volunteerId = $volunteer[0];
        $username = TelegramHelper::usernameFromLink($volunteer[3]);
        $telegramLink = TelegramHelper::linkByUsername($username);
        $hyperlink = GoogleSpreadsheetsHelper::createHyperlink("@{$username}", $telegramLink);

        $data = [ $hyperlink, $taskId, '', '', '', $messageId, '', '', $volunteerId ];

        $this->sheetsService->addRow($this->sheetName, ...$data);
    }

    public function assignVolunteerToTask(string $volunteerId, string $taskId): void
    {
        [$rowIdx,] = $this->findAssignRequest($volunteerId, $taskId);

        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 2, Carbon::now()->toDateTimeString());
    }

    public function rejectTaskByVolunteer(string $volunteerId, string $taskId): void
    {
        [$rowIdx,] = $this->findAssignRequest($volunteerId, $taskId);

        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 7, Carbon::now()->toDateTimeString());
    }

    public function updateAdminMessage(string $volunteerId, string $taskId, string $adminMessageId): void
    {
        [$rowIdx,] = $this->findAssignRequest($volunteerId, $taskId);

        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 6, $adminMessageId);
    }


    public function approveVolunteerRequest(string $volunteerId, string $taskId, string $adminLink): array
    {
        [$rowIdx, $assignRequest] = $this->findAssignRequest($volunteerId, $taskId);

        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 3, Carbon::now()->toDateTimeString());
        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 4, $adminLink);

        return $assignRequest;
    }

    public function findAssignRequest(string $volunteerId, string $taskId): ?array
    {
        $assignRequests = $this->sheetsService->getValues($this->sheetName, count($this->columns));

        foreach ($assignRequests as $rowIdx => $assignRequest) {
            if ($assignRequest[8] == $volunteerId && $assignRequest[1] == $taskId) {
                return [$rowIdx, $assignRequest];
            }
        }

        return null;
    }
}
