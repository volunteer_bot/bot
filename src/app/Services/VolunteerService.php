<?php

namespace App\Services;

use App\Helpers\PhoneHelper;
use Carbon\Carbon;

class VolunteerService
{
    private SheetsService $sheetsService;
    private string $sheetName;
    private array $columns;

    public function __construct(SheetsService $sheetsService)
    {
        $this->sheetsService = $sheetsService;
        $this->sheetName = config('sheets.volunteers.sheetName');
        $this->columns = config('sheets.volunteers.columns');
    }

    public function isVolunteerApproved(string $phoneNumber): bool
    {
        [, $volunteer] = $this->findVolunteerByPhone($phoneNumber);

        return !empty($volunteer) && !empty($volunteer[6]);
    }

    public function isVolunteerExists(string $phoneNumber): bool
    {
        [, $volunteer] = $this->findVolunteerByPhone($phoneNumber);

        return !empty($volunteer);
    }

    public function findVolunteerByPhone(string $phoneNumber): ?array
    {
        $volunteers = $this->findVolunteers();
        $phoneNumber = PhoneHelper::normalizePhoneNumber($phoneNumber);

        foreach ($volunteers as $rowIdx => $volunteer) {
            $volunteerPhoneNumber = PhoneHelper::normalizePhoneNumber($volunteer[2]);

            if ($volunteerPhoneNumber === $phoneNumber) {
                return [$rowIdx, $volunteer];
            }
        }

        return null;
    }

    public function findVolunteerByTelegramId(string $telegramId): ?array
    {
        $volunteers = $this->findVolunteers();

        foreach ($volunteers as $rowIdx => $volunteer) {
            if ($volunteer[4] == $telegramId) {
                return [$rowIdx, $volunteer];
            }
        }

        return null;
    }

    public function findVolunteerById(string $volunteerId): ?array
    {
        $volunteers = $this->findVolunteers();

        foreach ($volunteers as $rowIdx => $volunteer) {
            if ($volunteer[0] == $volunteerId) {
                return [$rowIdx, $volunteer];
            }
        }

        return null;
    }

    public function findVolunteers(): array
    {
        $result = [];

        $data = $this->sheetsService->getValues($this->sheetName, count($this->columns));

        foreach ($data as $idx => $row) {
            if ($idx === 0) {
                continue;
            }

            $result[$idx] = $row;
        }

        return $result;
    }

    public function approveVolunteer(string $volunteerId, string $adminUserLink)
    {
        [$rowIdx, $volunteer] = $this->findVolunteerById($volunteerId);

        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 6, Carbon::now()->toDateTimeString());
        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 7, $adminUserLink);

        return $volunteer;
    }

    public function updateVolunteer(string $phoneNumber, array $data)
    {
        [$rowIdx, $volunteer] = $this->findVolunteerByPhone($phoneNumber);

        $volunteer = array_replace($volunteer, $data);

        $volunteer[0] = intval($volunteer[0]);

        $this->sheetsService->updateRow($this->sheetName, $rowIdx, ...$volunteer);
    }


    public function addApproveVolunteerMessageId(string $volunteerId, string $messageId)
    {
        [$rowIdx,] = $this->findVolunteerById($volunteerId);

        $this->sheetsService->updateCell($this->sheetName, $rowIdx, 8, $messageId);
    }

    public function createVolunteer(array $data)
    {
        array_unshift($data, '=ROW()-1');
        $data[6] = $data[7] = '';

        $this->sheetsService->addRow($this->sheetName, ...$data);
    }
}
