<?php

namespace App\Console\Commands;

use App\Services\PublishService;
use App\Services\TaskService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class ContinuePublishTask extends Command
{
    public $signature = 'bot:continue_publish_task {taskId} {--messageId=} {--startFrom=1}';

    public function handle(Api $telegram, TaskService $taskService)
    {
        $publishService = app(PublishService::class, [
            'telegram' => $telegram,
        ]);

        $taskId = $this->argument('taskId');
        $startFrom = $this->option('startFrom');
        $messageId = $this->option('messageId');

        $this->confirm("Publish task {$taskId} from {$startFrom} user?");

        $publishService->publishTask(config('telegram.groupId'), $taskId, $startFrom);

        if (!empty($messageId)) {
            $telegram->editMessageReplyMarkup([
                'chat_id' => config('telegram.groupId'),
                'message_id' => $messageId,
                'reply_markup' => json_encode([
                    'inline_keyboard' => [
                        [
                            [
                                'text' => "\xe2\x9c\x85\x0a " . __('telegram.task_published'),
                                'url' => $taskService->getRangeLinkByTaskId($taskId),
                            ],
                        ],
                        [
                            [
                                'text' => __('telegram.finish_task'),
                                'callback_data' => "finishTask:{$taskId}",
                            ],
                        ],
                    ],
                    'resize_keyboard' => true,
                ]),
            ]);
        }
    }
}
