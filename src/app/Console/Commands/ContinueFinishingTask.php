<?php

namespace App\Console\Commands;

use App\Services\PublishService;
use App\Services\TaskService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class ContinueFinishingTask extends Command
{
    public $signature = 'bot:continue_finish_task {taskId} {--messageId=} {--startFrom=1}';

    public function handle(Api $telegram, TaskService $taskService)
    {
        $publishService = app(PublishService::class, [
            'telegram' => $telegram,
        ]);

        $taskId = $this->argument('taskId');
        $startFrom = $this->option('startFrom');
        $messageId = $this->option('messageId');

        $this->confirm("Finish task {$taskId} from {$startFrom} user?");

        $publishService->finishTask($taskId, config('telegram.groupId'), $startFrom);

        $taskService->updateFinishedAtFieldForTask($taskId);

        if (!empty($messageId)) {
            $telegram->editMessageReplyMarkup([
                'chat_id' => config('telegram.groupId'),
                'message_id' => $messageId,
                'reply_markup' => json_encode([
                    'inline_keyboard' => [
                        [
                            [
                                'text' => "\xf0\x9f\x8f\x81 " . __('telegram.task_finished'),
                                'url' => $taskService->getRangeLinkByTaskId($taskId),
                            ],
                        ],
                    ],
                    'resize_keyboard' => true,
                ]),
            ]);
        }
    }
}
