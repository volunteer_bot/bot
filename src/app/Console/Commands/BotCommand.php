<?php

namespace App\Console\Commands;

use App\Helpers\TelegramHelper;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Update;
use Throwable;
use function Sentry\captureException;

class BotCommand extends Command
{
    const DELAY = 1000;

    protected $signature = 'bot';

    public function handle(Api $telegram)
    {
        $this->info("start");

        while (true) {
            try {
                $this->update($telegram);
                usleep(self::DELAY);
            } catch (Throwable $e) {
                if (config('app.debug')) {
                    throw $e;
                } else {
                    $this->error($e->getMessage());
                    captureException($e);
                }
            }
        }
    }

    protected function update(Api $telegram): bool
    {
        $updates = $telegram->commandsHandler(false);

        foreach ($updates as $update) {
            if (!$this->lock($update)) {
                $this->warn('operation has already been started!');
                continue;
            }

            $this->printInfo($update);

            if ($update->isType('message')) {
                switch ($update->getMessage()->detectType()) {
                    case 'contact':
                        $telegram->triggerCommand('checkContact', $update);
                        break;
                }

                if (in_array($update->getChat()->type, ['group', 'supergroup'])) {
                    $telegram->triggerCommand('supergroup', $update);
                }
            } elseif ($update->isType('callback_query')) {
                $data = explode(':', $update->callbackQuery->data);

                if (count($data) < 2) {
                    continue;
                }

                switch ($data[0]) {
                    case 'approveVolunteer':
                        $telegram->triggerCommand('approveVolunteer', $update, [
                            'userId' => $data[1],
                        ]);

                        break;
                    case 'createTask':
                        $telegram->triggerCommand('createTask', $update, [
                            'userId' => $data[1],
                        ]);

                        break;
                    case 'createInfoMessage':
                        $telegram->triggerCommand('createInfoMessage', $update, [
                            'userId' => $data[1],
                        ]);

                        break;
                    case 'publishTask':
                        $telegram->triggerCommand('publishTask', $update, [
                            'taskId' => $data[1],
                        ]);

                        break;
                    case 'finishTask':
                        $telegram->triggerCommand('finishTask', $update, [
                            'taskId' => $data[1],
                        ]);

                        break;
                    case 'publishInfoMessage':
                        $telegram->triggerCommand('publishInfoMessage', $update, [
                            'messageId' => $data[1],
                        ]);

                        break;
                    case 'replyTask':
                        $telegram->triggerCommand('replyTask', $update, [
                            'taskId' => $data[1],
                        ]);

                        break;
                    case 'rejectTask':
                        $telegram->triggerCommand('rejectTask', $update, [
                            'taskId' => $data[1],
                        ]);

                        break;
                    case 'info':
                        $telegram->triggerCommand('info', $update, [
                            'message' => $data[1],
                        ]);

                        break;
                    case 'approveAssign':
                        $telegram->triggerCommand('approveAssign', $update, [
                            'taskId' => $data[1],
                            'volunteerId' => $data[2],
                        ]);

                        break;
                }
            }
        }

        return !empty($updates);
    }

    private function lock(Update $update): bool
    {
        if ($update->detectType() !== 'callback_query') {
            return true;
        }

        $query = $update->callbackQuery;

        $hash = $query->get('data');

        $message = $query->get('message');

        if (!empty($message)) {
            $messageId = $message->get('message_id');

            if (!empty($messageId)) {
                $hash .= ':' . $messageId;
            }
        }

        return TelegramHelper::lock($hash);
    }

    private function printInfo(Update $update): void
    {
        $type = $update->detectType();

        switch ($type) {
            case 'callback_query':
                $query = $update->callbackQuery;
                $username = $query->from->username;
                $group = $query->message->chat->title;
                $data = $query->data;

                if ($group) {
                    $this->info("[admin][callback_query] from @{$username} in group `{$group}`; data: `{$data}`");
                } else {
                    $this->info("[callback_query] from @{$username}; data: `{$data}`");
                }

                break;
            case 'message':
                $subType = $update->message->detectType();

                if (in_array($update->getChat()->type, ['group', 'supergroup'])) {
                    $username = $update->getMessage()->from->username;

                    $this->info("[admin] new message from @{$username}");
                } elseif ($subType === 'contact') {
                    $username = $update->getChat()->username;

                    $this->info("[check_contact] @{$username}");
                }

                break;
        }
    }

    /**
     * Write a string as standard output.
     *
     * @param  string  $string
     * @param  string|null  $style
     * @param  int|string|null  $verbosity
     * @return void
     */
    public function line($string, $style = null, $verbosity = null)
    {
        $time = Carbon::now()->toDateTimeString();

        $styled = $style ? "<$style>[$time] $string</$style>" : "[$time] $string";

        $this->output->writeln($styled, $this->parseVerbosity($verbosity));
    }
}
