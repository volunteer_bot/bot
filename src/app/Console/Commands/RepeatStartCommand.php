<?php

namespace App\Console\Commands;

use App\Helpers\TelegramHelper;
use App\Services\VolunteerService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class RepeatStartCommand extends Command
{
    public $signature = 'bot:repeat_start {volunteerId}';

    public function handle(VolunteerService $volunteerService, Api $telegram)
    {
        $volunteerId = $this->argument('volunteerId');

        [, $volunteer] = $volunteerService->findVolunteerById($volunteerId);

        $message = $telegram->sendMessage([
            'chat_id' => config('telegram.groupId'),
            'text' => __('telegram.new_volunteer', [
                'volunteerId' => $volunteer[0],
                'fullName'    => TelegramHelper::escapeMarkdownV2($volunteer[1]),
                'phone'       => TelegramHelper::escapeMarkdownV2($volunteer[2]),
                'userLink'    => TelegramHelper::linkByUsername($volunteer[3], true),
            ]),
            'parse_mode' => 'MarkdownV2',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => __('telegram.approve_volunteer'),
                            'callback_data' => "approveVolunteer:{$volunteer[0]}",
                        ],
                    ],
                ],
            ]),
        ]);

        $volunteerService->addApproveVolunteerMessageId($volunteer[0], $message->messageId);
    }
}
