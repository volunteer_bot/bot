<?php

namespace App\Console\Commands;

use App\Services\SheetsService;
use Illuminate\Console\Command;

class MigrateCommand extends Command
{
    protected $signature = 'bot:migrate';

    public function handle(SheetsService $sheetsService)
    {
        foreach (config('sheets') as $sheet) {
            $sheetsService->createOrUpdateSheet($sheet['sheetName'], ...$sheet['columns']);

            foreach ($sheet['columns'] as $columnIdx => $columnName) {
                if (in_array($columnName, $sheet['hidden'] ?? [])) {
                    $sheetsService->hideColumn($sheet['sheetName'], $columnIdx);
                }
            }
        }
    }
}
