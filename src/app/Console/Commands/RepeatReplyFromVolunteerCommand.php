<?php

namespace App\Console\Commands;

use App\Helpers\TelegramHelper;
use App\Services\AssignService;
use App\Services\TaskService;
use App\Services\VolunteerService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class RepeatReplyFromVolunteerCommand extends Command
{
    public $signature = 'bot:repeat_reply {taskId} {volunteerId}';

    public function handle(
        VolunteerService $volunteerService,
        AssignService $assignService,
        TaskService $taskService,
        Api $telegram)
    {
        $taskId = $this->argument('taskId');
        $volunteerId = $this->argument('volunteerId');

        list(, $volunteer) = $volunteerService->findVolunteerById($volunteerId);

        $assignService->assignVolunteerToTask($volunteerId, $taskId);
        [, $assign] = $assignService->findAssignRequest($volunteerId, $taskId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $volunteer[4],
            'message_id' => $assign[5],
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => "\xe2\x8f\xb3\x0a " . __('telegram.you_replied'),
                            'callback_data' => 'info:replyTask',
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);

        $task = $taskService->getTaskById($taskId);

        $messageId = $task[2];

        $adminGroupId = config('telegram.groupId');

        // todo: better link generation
        $taskLink = "https://t.me/c/" . str_replace('-100', '', $adminGroupId) . "/{$messageId}";
        $userLink = TelegramHelper::linkByUsername($volunteer[3], true);

        $message = $telegram->sendMessage([
            'chat_id' => config('telegram.groupId'),
            'text' => __('telegram.new_assign_requested', [
                'volunteerId' => $volunteer[0],
                'fullName' => TelegramHelper::escapeMarkdownV2($volunteer[1]),
                'phone' => TelegramHelper::escapeMarkdownV2($volunteer[2]),
                'userLink' => $userLink,
                'taskLink' => $taskLink,
            ]),
            'parse_mode' => 'MarkdownV2',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => __('telegram.approve_assign_request'),
                            'callback_data' => "approveAssign:{$taskId}:{$volunteerId}",
                        ],
                    ],
                ],
            ]),
            'disable_web_page_preview' => true,
            'reply_to_message_id' => $messageId,
        ]);

        $assignService->updateAdminMessage($volunteerId, $taskId, $message->messageId);
    }
}
