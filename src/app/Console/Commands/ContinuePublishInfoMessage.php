<?php

namespace App\Console\Commands;

use App\Services\InfoMessageService;
use App\Services\PublishService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class ContinuePublishInfoMessage extends Command
{
    public $signature = 'bot:continue_publish_info_message {infoMessageId} {--messageId=} {--startFrom=1}';

    public function handle(Api $telegram, InfoMessageService $infoMessageService)
    {
        $publishService = app(PublishService::class, [
            'telegram' => $telegram,
        ]);

        $infoMessageId = $this->argument('infoMessageId');
        $startFrom = $this->option('startFrom');

        $messageId = $this->option('messageId');

        $this->confirm("Publish info message {$infoMessageId} from {$startFrom} user?");

        $publishService->publishInfoMessage(config('telegram.groupId'), $infoMessageId, $startFrom);

        if (!empty($messageId)) {
            $telegram->editMessageReplyMarkup([
                'chat_id' => config('telegram.groupId'),
                'message_id' => $messageId,
                'reply_markup' => json_encode([
                    'inline_keyboard' => [
                        [
                            [
                                'text' => "\xe2\x9c\x85\x0a " . __('telegram.info_message_published'),
                                'url' => $infoMessageService->getRangeLinkByInfoMessageId($infoMessageId),
                            ],
                        ],
                    ],
                    'resize_keyboard' => true,
                ]),
            ]);
        }
    }
}
