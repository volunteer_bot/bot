<?php

namespace App\Console\Commands;

use App\Services\AssignService;
use App\Services\VolunteerService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class RepeatRejectFromVolunteerCommand extends Command
{
    public $signature = 'bot:repeat_reject {taskId} {volunteerId}';

    public function handle(
        VolunteerService $volunteerService,
        AssignService $assignService,
        Api $telegram)
    {
        $taskId = $this->argument('taskId');
        $volunteerId = $this->argument('volunteerId');

        list(, $volunteer) = $volunteerService->findVolunteerById($volunteerId);

        $assignService->rejectTaskByVolunteer($volunteerId, $taskId);
        [, $assign] = $assignService->findAssignRequest($volunteerId, $taskId);

        $telegram->editMessageReplyMarkup([
            'chat_id' => $volunteer[4],
            'message_id' => $assign[5],
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => __('telegram.you_rejected'),
                            'callback_data' => 'info:rejectedTask',
                        ],
                    ],
                ],
                'resize_keyboard' => true,
            ]),
        ]);
    }
}
