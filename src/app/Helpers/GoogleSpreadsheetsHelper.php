<?php

namespace App\Helpers;

class GoogleSpreadsheetsHelper
{
    public static function createHyperlink(string $text, string $link)
    {
        if (empty($link)) {
            return '';
        }

        return "=HYPERLINK(\"$link\";\"$text\")";
    }
}
