<?php

namespace App\Helpers;

class TelegramHelper
{
    public static function escapeMarkdownV2(?string $message)
    {
        /*
         * In all other places characters
         * '_', '*', '[', ']', '(', ')', '~', '`',
         * '>', '#', '+', '-', '=', '|', '{', '}',
         * '.', '!'
         * must be escaped with the preceding character '\'.
         */

        $symbols = [
            '_', '*', '[', ']', '(', ')', '~', '`', '>',
            '#', '+', '-', '=', '|', '{', '}', '.', '!',
        ];

        $replacements = [
            '\_', '\*', '\[', '\]', '\(', '\)', '\~', '\`', '\>',
            '\#', '\+', '\-', '\=', '\|', '\{', '\}', '\.', '\!',
        ];

        return str_replace($symbols, $replacements, (string) $message);
    }

    public static function usernameFromLink(string $link): string
    {
        // @testuser
        if (substr($link, 0, 1) === '@') {
            return substr($link, 1);
        }

        // https://t.me/testuser
        $tme = strpos($link, 't.me/');

        if ($tme !== false) {
            return substr($link, $tme + 5);
        }

        return '';
    }

    public static function linkByUsername(?string $username, bool $markdown = false): string
    {
        if (empty($username)) {
            return '';
        }

        $link = "https://t.me/{$username}";

        if (!$markdown) {
            return $link;
        }

        $username = TelegramHelper::escapeMarkdownV2($username);

        return "[{$username}]({$link})";
    }

    protected static $files = [];

    public static function lock(string $identifier): bool
    {
        $file = fopen(storage_path("locks/{$identifier}.lock"), 'w');

        if (!flock($file, LOCK_EX | LOCK_NB)) {
            return false;
        }

        static::$files[$identifier] = $file;

        return true;
    }
}
