<?php

namespace App\Helpers;

use JetBrains\PhpStorm\Pure;

class PhoneHelper
{
    #[Pure]
    public static function normalizePhoneNumber(string $phoneNumber): string
    {
        $phoneNumber = preg_replace("/[^\d]*/", '', $phoneNumber);

        if (in_array(substr($phoneNumber, 0, 1), ['+', '8'])) {
            $phoneNumber = substr_replace($phoneNumber, '7', 0, 1);
        }

        if (substr($phoneNumber, 0, 1) !== '7') {
            $phoneNumber = "7{$phoneNumber}";
        }

        return $phoneNumber;
    }
}
