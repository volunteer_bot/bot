<?php

namespace App\Exceptions;

use Telegram\Bot\Objects\Update;

class TelegramUpdateSerializer
{
    public function __invoke(Update $update): array
    {
        return $update->toArray();
    }
}
