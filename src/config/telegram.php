<?php

use App\Telegram\Commands;

return [
    'default' => 'volunteerBot',
    'bots' => [
        'volunteerBot' => [
            'username' => env('TELEGRAM_BOT_NAME', 'Volunteer Bot'),
            'token'    => env('TELEGRAM_BOT_TOKEN'),
            'commands' => [
                Commands\StartCommand::class,
                Commands\CheckContactCommand::class,
                Commands\SuperGroup\SuperGroupCommand::class,
                Commands\SuperGroup\CreateTaskCommand::class,
                Commands\SuperGroup\PublishTaskCommand::class,
                Commands\SuperGroup\FinishTaskCommand::class,
                Commands\SuperGroup\CreateInfoMessageCommand::class,
                Commands\SuperGroup\PublishInfoMessageCommand::class,
                Commands\ReplyTaskCommand::class,
                Commands\InfoCommand::class,
                Commands\SuperGroup\ApproveAssignCommand::class,
                Commands\SuperGroup\ApproveVolunteerCommand::class,
                Commands\RejectTaskCommand::class,
            ],
        ],
    ],
    'commands' => [
        Commands\HelpCommand::class,
    ],
    'groupId' => env('TELEGRAM_GROUP_ID'),
];
