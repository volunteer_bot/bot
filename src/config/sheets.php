<?php

return [
    'volunteers' => [
        'sheetName' => 'Volunteers',
        'columns'   => [
            'ID',
            'ФИО',
            'Телефон',
            'Telegram Login',
            'Telegram ID',
            'Created At',
            'Approved At',
            'Approved By',
            'Approve Message ID',
        ],
        'hidden'   => [
            'Created At',
            'Approved At',
            'Approve Message ID',
        ],
        'redisKey' => 'Телефон',
    ],
    'tasks' => [
        'sheetName' => 'Tasks',
        'columns'   => [
            'ID',
            'Краткое название',
            'Telegram Message Link',
            'Created By',
            'Created At',
            'Published At',
            'Finished At',
        ],
        'hidden'   => [
            'Created At',
            'Published At',
            'Finished At',
        ],
        'redisKey' => 'Telegram Message Link',
    ],
    'info_messages' => [
        'sheetName' => 'Info Messages',
        'columns'   => [
            'ID',
            'Краткое название',
            'Telegram Message Link',
            'Created By',
            'Created At',
            'Published At',
        ],
        'hidden'   => [
            'Created At',
            'Published At',
        ],
        'redisKey' => 'Telegram Message Link',
    ],
    'assigns' => [
        'sheetName' => 'Assigns',
        'columns'   => [
            'Volunteer Telegram',
            'Task ID',
            'Requested At',
            'Approved At',
            'Approved By',
            'Message ID',
            'Admin Message ID',
            'Rejected At',
            'Volunteer ID',
        ],
        'hidden'   => [
            'Requested At',
            'Approved At',
            'Message ID',
            'Admin Message ID',
            'Rejected At',
            'Volunteer ID',
        ],
        'redisKey' => ['Volunteer ID'],
    ],
    /*
    'admins' => [
        'sheetName' => 'Admins',
        'columns'   => [
            'ID',
            'ФИО',
            'Телефон',
            'Telegram Login',
            'Telegram ID',
            'Created At',
        ],
        'redisKey' => 'Телефон',
    ],
    'configuration' => [
        'sheetName' => 'Configuration',
        'columns'   => [
            'Key',
            'Value',
        ],
        'redisKey' => 'Key',
    ],
    */
];
